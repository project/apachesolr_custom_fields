If you are using Apachesolr module, 
this module won't add all of the cck to apachesolr server.
So you need to add your custom code to add these fields.

The module, Apachesolr Custom Fields, 
will help you to do this easily.
Also it could help you to create custom fields with some settings.

apachesolr_custom_field-1.0 RC 
2011-10-4

1. Support normal CCK field. Single vaule or Multi value.
2. Support custom field. Single value or Multi value.


NOTICE
The custom field callback support PHP code, then there could be
risk to your server, so if you want don't want it, you can use
'administer search phpmode' perm to restrict it.

