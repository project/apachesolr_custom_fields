CONTENTS OF THIS FILE
---------------------

 * Requirements and notes
 * Installation
 * Add CCK or custom fields
 * More information


REQUIREMENTS AND NOTES
----------------------
1. Drupal-6.x
2. Apachesolr
3. Apachesolr_search
4. Content 

INSTALLATION
------------ 
1. Download and extract to Drupal module folder.
2. Enable apachesolr_custom_field
3. Go to admin/settings/apachesolr/custom-fields
   it will list all CCK fields and a link which navigates to add a custom field.


Add CCK/Custom Fields to Solr
----------------------------
Go to admin/settings/apachesolr/custom-fields
Will list all CCK/custom fileds, you can enable or disable them.
Click 'Add cutome field' to go to a page that you can add custom fields.

NOTICE
The custom field callback support PHP code, then there could be
risk to your server, so if you want don't want it, you can use
'administer search phpmode' perm to restrict it.

MORE INFORMATION
---------------
Please go to http://www.drupal.org/project/apachesolr_custom_fields to get more information

author: robbin.joe@gmail.com
